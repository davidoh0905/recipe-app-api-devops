#!/bin/sh
set -e # bubble up the error!

# in Django, it is recommended to serve static files via proxy.
# uWSGI is really good at serving python application. executing python code and getting response.
# But not optimized to serve static files. serve from proxy.

# How to get static files from Django to Proxy?
# - Collect Static : Django will collect all static files into a folder
# - Allows you to collect all of the static files required for your project
# - and store them into a single directory

# 1. Collect all static files
python manage.py collectstatic --noinput # assume yes to everything since this is running in server

# 2. Run Migrations
# 2.1 Make sure database is available to run migrations
# - Django's Database Migrations
# - Migration file to make necessary changes to the database.
# - adding / removing table
# polls db every second. when available and it continues
python manage.py wait_for_db
# 2.2 Run migrations on database
python manage.py migrate

####### Fianlly run uWSGI #####
# CGI: Common Gateway Interface
# - webservers receive requests via instance port
# - translating the request into a format that the application can understand
# - and then send the response back to the webserver
# 웹서버랑 파이썬 코드랑 통신

uwsgi --socket :9000 --workers 4 --master --enable-thread --module app.wsgi
# - "uwsgi" is the name of the application we are going to run. bring up new uwsgi server.
# - socket :9000 -> run uwsgi server as a tcp socket on port 9000. <-- what does it mean?
# - - Map the request from our proxy to this port, using uwsgi path that we set up in nginx proxy
# - number of workers for uwsgi service. based on memory and resources.
# - - you can have one worker and have multiple docker containers to handle the load
# - - But recommended to have more than 1 worker in a container. 4 workers in our container....
# - - Question : What are the workers really?
# - master flag -> run this as master service on terminal
# - ...exit -> gracefully close
# - enable-thread -> enable threading in uwsgi
# - module : application uWSGI is going to run
# - - There are actual file app/wsgi.py
# - - serving our application as uWSGI service.
# - - Are NGINX proxy and uWSGI different stuff?
# - - it is going to run the wsgi.py file in app folder

# entrypoint.sh is a script that Docker will run
# in order to start the application in Docker container

#### Now we need to make Dockerfile run the entrypoint script
