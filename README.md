# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

## Test The Docker image build
```docker build . ```

Testing within bastion

[ec2-user@ip-10-1-1-65 ~]$ docker run -it -e DB_HOST=raad-staging-db.c3agf8nivrsa.us-east-1.rds.amazonaws.com -e DB_NAME=recipe -e DB_USER=recipeapp -e DB_PASS=changeme123 340001926556.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest sh -c "python manage.py wait_for_db && python manage.py createsuperuser"

- -it : interactive mode
- DB_HOST, DB_NAME, DB_USER, DB_PASS : all required parameters which are currently provided by gitlab
- sh -c : providing shell script
- create super user!
[ec2-user@ip-10-1-1-65 ~]$ sudo docker run -it -e DB_HOST=raad-staging-db.c3agf8nivrsa.us-east-1.rds.amazonaws.com -e DB_NAME=recipe -e DB_USER=recipeapp -e DB_PASS=changeme123 340001926556.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest sh -c "python manage.py wait_for_db && python manage.py createsuperuser"

And then use the public ip and access publicIp:8000/admin
With the superuser, this will work!
How the hell is this working tho...