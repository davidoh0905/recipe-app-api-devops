# output of a certain variable or attribute on one of our resource
# output of database address!
# configure & debugging our server
output "db_host" {
  # Internal network address for our database
  value = aws_db_instance.main.address
}

## Every time you deploy new bastion instance, it will have a new (endpoint) public IP address.
## That is the host name we can use to connect to instance from the internet.
## useful to be able to output this to terraform!!! obviously!!
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# We get below in the log! 
# Outputs:
# bastion_host = ec2-52-91-52-72.compute-1.amazonaws.com
# db_host = raad-staging-db.c3agf8nivrsa.us-east-1.rds.amazonaws.com
## ssh ec2-user@ec2-52-91-52-72.compute-1.amazonaws.com


output "api_endpoint" {
  #   value = aws_lb.api.dns_name
  value = aws_route53_record.app.fqdn
}