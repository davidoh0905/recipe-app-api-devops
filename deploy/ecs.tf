# Simple cluster!
# 1 cluster per environment
resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"
  tags = local.common_tags
}

## Below 3 resources are for assigning permissions in order to START a new task.
# Policy : aws_iam_policy.task_execution_role_policy
# Role : aws_iam_role.task_execution_role
# Role & Policy attachment : aws_iam_role_policy_attachment

# Create resources for managing these policies
# This creates a new IAM policy in AWS account.
# This is policy for task execution! And it will need to be attached to the role.
# This will give ECS, a permission to retrieve images from ECR and add logs to CloudWatch.
# In order to start a new task!

# Allows our ECS task to retrieve the image from 
# ECR and put logs into the log stream

# Task execution role is a role used for starting a service.
# Two types of role assigned to ECS task:
# - Role 1 : Start the service. Like AWS resources that it needs to access in order to start a new task.
# - Role 2 : Permissions to the running task itself. Needed at run time. e.g. S3 bucket access 
# - Current file is for Role 2.


resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving images and adding to logs"
  # Content of the policy is in the file.
  policy = file("./templates/ecs/task-exec-role.json")
}


# This allows our ECS tasks to assume the role defined in task-exec-role
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")
}

# Attach the policy to the role.
# Weird how "attachment" itself is a role...
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}


## aws_iam_role.app_iam_role is going to be used for giving permissions to our task
## that it needs at RUNTIME. What running docker container needs!
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"
  tags = local.common_tags
}

## Cointainer Definition Template
## A JSON file which contains all the details of our container so AWS
## knows how to run it in production.
## - Image registry & image tag
## - Memory to assign to it
## - Different configuration options(i.e. environment variables)

## Copy the template from : terraform.io/docs/providers/aws/r/ecs_task_definition.html#example-usage

# Template for our container definition
data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definition.json.tpl")
  vars = {
    # var.ecr_image_api will be pulled from variables.tf
    # URI for images in our ECR
    app_image   = var.ecr_image_api
    proxy_image = var.ecr_image_proxy
    # we will set this via the environment variables. probably TF_VAR_django_secret_key
    # passed in from gitlab CI/CD process
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    # These eventually also come from gitlab via env variables
    db_user          = aws_db_instance.main.username
    db_pass          = aws_db_instance.main.password
    log_group_name   = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region = data.aws_region.current.name
    # LB hosts! 
    # allowed_hosts            = aws_lb.api.dns_name
    allowed_hosts            = aws_route53_record.app.fqdn
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

# We will create resource that will create our task definition
# Task definition of our api
resource "aws_ecs_task_definition" "api" {
  # "family" is a name for the task definition.
  family = "${local.prefix}-api"
  # This retrieves rendered container definition!
  container_definitions = data.template_file.api_container_definitions.rendered
  # We want to make our ECS task compatible with FARGATE
  # Serverless version of deploying containers.
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  # Different combination is available!!
  cpu    = 256
  memory = 512
  # TASK START Gives the permission to execute a new task
  execution_role_arn = aws_iam_role.task_execution_role.arn
  # TASK RUNTIME
  task_role_arn = aws_iam_role.app_iam_role.arn
  volume {
    name = "static"
  }
  tags = local.common_tags
}

## Now actually defining task?
## 
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  egress {
    # Allow outbound access from our container on port 443
    # access to network to pull containers from ECR.
    # and any output traffic via 443
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    # Allow outbound from our service on port 5432 
    # to out private subnet that the database is running in.
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  ingress {
    # Allow inbound connections from public internet to proxy
    # only allow to 8000 which is the port that our proxy is running on
    # we don't allow on port 9000 where our application is running on.
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    # Allow Tasks to register to LB
    security_groups = [aws_security_group.lb.id]
    # cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name = "${local.prefix}-api"
  # Assign the cluster!
  cluster = aws_ecs_cluster.main.name
  # Task Definition!
  task_definition = aws_ecs_task_definition.api.family
  # Number of tasks that we wish to run inside service
  desired_count = 3
  # Allows you to run takss without managing a cluster
  launch_type      = "FARGATE"
  platform_version = "1.4.0"
  network_configuration {
    subnets = [
      # Now only accepting requests via LB
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [
      aws_security_group.ecs_service.id,
    ]
    # Assign a public IP address to a service
    # assign_public_ip = true
    # No longer needed!
  }

  # Tells our ECS service to register new tasks with our target group!!
  # Forward to the proxy! at port 8000
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }
  depends_on = [
    aws_lb_listener.api_https
  ]
}

data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the recipe app S3 bucket"
  policy      = data.template_file.ecs_s3_write_policy.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}

