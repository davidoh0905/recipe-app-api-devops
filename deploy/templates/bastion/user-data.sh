#!/bin/bash

{% comment %} update our yum pakcage maanger with latest packages {% endcomment %}
sudo yum update -y
{% comment %} install docker {% endcomment %}
sudo amazon-linux-extras install -y docker
{% comment %} enable and start docker {% endcomment %}
{% comment %} But do we have docker file in our bastion server? {% endcomment %}
sudo systemctl enable docker.service
sudo systemctl start docker.service
{% comment %} add our EC2 user to the Docker group {% endcomment %}
{% comment %} run and manage docker through that account.
Okay I don't know this part {% endcomment %}
sudo usermod -aG docker ec2-user


{% comment %} User Data Script is script that is run when you first deploy your instance.
When deploying EC2 instance for my bastion server, you can run script that can install dependency 
needed for administering AWS reousrces {% endcomment %}
{% comment %} Pass into resources in AWS {% endcomment %}