resource "aws_vpc" "main" {
  # block that indicates IP addresses that will be available in our network
  # We want VPC with most amount of available resources.
  # define our network to begin with 10.1
  # 2 divider network into...what??
  cidr_block = "10.1.0.0/16"
  # dns inside our vpc
  enable_dns_support = true
  # hostnames in any resources created in our vpc
  enable_dns_hostnames = true
  # tags
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-vpc")
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

#####################################################
# Public Subnets - Inbound/Outbound Interent Access #
#####################################################
# Public Subnets are used to give resources access to the internet publicly
# Resource is accessible from public internet
# e.g. load balancer, bastion server, and etc
# /24 netmask gives us 254 IP addresses
# netmaek 255.255.255.0 idk...
# Per Subnet, we can have 254 hosts!
# A Side of our public subnet.

# Am I creating a subnet for each availability zone?
resource "aws_subnet" "public_a" {
  vpc_id = aws_vpc.main.id
  # everything in this subnet is going to start with 10.1.1
  # 10.1.1.1 --> first resource
  # 10.1.1.2 --> second resource and etc
  cidr_block = "10.1.1.0/24"
  # Anything we add to this subnet will get a public ip address
  map_public_ip_on_launch = true
  # availability zone is a way of dividing regions up into separate zones
  # so that if one zone goes down, 
  availability_zone = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

## Having subnets in multiple availability zones is required for load balancer.
## Route table is a way of adding routes to a subnet.
## way of adding route to a subnet...
resource "aws_route_table" "public_a" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}


### aws_route and aws_route_table_association do not support tags!
### Because they are associated with other resources that does have tags.
### both are associated with route table that has tags!
###           route_table
###           /    \
###          /      \
###     aws_route  association
###     /               \
### gateway_id        subnet_id

# Creates association between our route table and subnet
# I guess... route table & subnet ..
resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_a.id
}

# Add our route?
# Making subnet accessible via internet gateway
# This is a route out to the internet.
# So... all of the internet traffic will go through internet gateway.
# And internet gateway will use the route table and route all traffic to route table
# and route table will route it to .... subnet?

# Adding actual route to route table!!!!!
resource "aws_route" "public_internet_access_a" {
  # The route table that we are adding route to!
  route_table_id = aws_route_table.public_a.id
  # All addresses(public internet)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

# EIP : Elastic IP
resource "aws_eip" "public_a" {
  vpc = true
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}
# NAT Gateway : Network Address Translation
# Outbound access!
resource "aws_nat_gateway" "public_a" {
  # Allocation of eip and that's the way of creating ip address in vpc
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

##################################
resource "aws_subnet" "public_b" {
  vpc_id = aws_vpc.main.id
  # A side of 10.1.1
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${data.aws_region.current.name}b"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_route_table" "public_b" {
  # one vpc per env!
  vpc_id = aws_vpc.main.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}

resource "aws_route" "public_internet_access_b" {
  route_table_id         = aws_route_table.public_b.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

# B side IP address
resource "aws_eip" "public_b" {
  vpc = true
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_nat_gateway" "public_b" {
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

###################################################
# Private Subnets - Outbound Interent Access Only #
###################################################
# Out Databases!!
# Adds layer of security!
resource "aws_subnet" "private_a" {
  vpc_id = aws_vpc.main.id
  # 10.1.10 for private network
  # 10.1.1 ~ 10.1.9 is reserved for public network
  cidr_block        = "10.1.10.0/24"
  availability_zone = "${data.aws_region.current.name}a"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id = aws_route_table.private_a.id
  # in order to provide only outbound internet access, and block inbound
  # need natgateway.
  # private network does not have any ip address, so we need to use internet gateway
  nat_gateway_id         = aws_nat_gateway.public_a.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_subnet" "private_b" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.1.11.0/24"
  availability_zone = "${data.aws_region.current.name}b"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}

resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}


resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private_b.id
  nat_gateway_id         = aws_nat_gateway.public_b.id
  destination_cidr_block = "0.0.0.0/0"
}
