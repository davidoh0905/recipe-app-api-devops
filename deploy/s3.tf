resource "aws_s3_bucket" "app_public_files" {
  bucket_prefix = "${local.prefix}-files"
  acl           = "public-read"
  # becareful because this will be deleted as well!
  force_destroy = true
}