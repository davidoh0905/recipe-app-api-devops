# Create subnet group that we can we for our DB
# AWS's special resource : aws_db_subnet_group
# - Allows you to add multiple subnets to your database.

resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# When setting up AWS Networking, we need to create a security group
# Security groups allow you to control the inbound and outbound
# access allowed to that resource.
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  # Security group to manage the RDS inbound access
  vpc_id = aws_vpc.main.id

  # 5432 is default port for postgres
  # ingress block controls what the ruls are for inbound access
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    ## Limit it down further!
    # Only allow access from the bastion host
    security_groups = [
      # Access will be limited to resources with security group specified here
      aws_security_group.bastion.id,
      # Give access to our ECS service from our database
      # Gotta do this on both sides!
      aws_security_group.ecs_service.id,
    ]
  }

  tags = merge(
    local.common_tags,
  )
}

## Database Instance !
resource "aws_db_instance" "main" {
  identifier        = "${local.prefix}-db"
  name              = "recipe"
  allocated_storage = 20
  storage_type      = "gp2"
  engine            = "postgres"
  engine_version    = "11.15"
  # type of database server we want to run
  # memory and cpu of instance
  instance_class       = "db.t2.micro"
  db_subnet_group_name = aws_db_subnet_group.main.name
  username             = var.db_username
  password             = var.db_password
  # back up days
  backup_retention_period = 0
  # Better resiliency if you set it to multiple AZs
  # But don't we have a, b AZ already set up?
  multi_az               = false
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
