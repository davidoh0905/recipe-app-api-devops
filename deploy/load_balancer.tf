## Three components of Load Balancer
# 1. Load Balancer
# 2. Target Group
# 3. Listener
# & Security Group

resource "aws_lb" "api" {
  name               = "${local.prefix}-api"
  load_balancer_type = "application"
  # ALB vs NLB
  # Application load balancer --> handle requests at the HTTP level(http & https)
  # Network load balancer --> handle requests at the network level(TCP & udp)
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id,
  ]
  security_groups = [
    aws_security_group.lb.id,
  ]
  tags = local.common_tags
}

resource "aws_lb_target_group" "api" {
  # Name of target group for our application 
  name = "${local.prefix}-api"
  # LB will send HTTP request to our api
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  port        = 8000 # port of proxy
  # Target Group : Group of servers that the LB can forward requests to

  # Health check
  health_check {
    path = "/admin/login"
  }
  tags = local.common_tags
}

# Listener is what accepts requests to our LB
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  # Listener will accept request on port 80
  port     = "80"
  protocol = "HTTP" # not HTTPS yet
  # And forwards to target group
  default_action {
    # type             = "forward"
    # target_group_arn = aws_lb_target_group.api.arn
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_lb.api.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = aws_acm_certificate_validation.cert.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}

# Security group to allow access load INTO our LB
resource "aws_security_group" "lb" {
  name        = "${local.prefix}-lb"
  description = "Allow access to application load balancer."
  vpc_id      = aws_vpc.main.id

  # Allows all connections from the public internet to out LB
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }
}

