variable "prefix" {
  default = "raad" # recipe app api devops
}

# These automated tags management is super helpful for managing many resources
# And when there are resources for different projects within same AWS account.
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "davidoh0905@gmail.com"
}

# These don't have default value. Let's see how it can be provided by gitlab secret!
variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
  # This matches up to the name of Key pair that we created via AWS console
  # of which value is the public SSH key.
  # This will be used when we SSH in from local machine to bastion host.
}

variable "ecr_image_api" {
  # Add the latest tag!
  default     = "340001926556.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
  description = "ECR image for API"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "340001926556.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secrek key for Django app"
}

# Retrieve zone name from our account
variable "dns_zone_name" {
  description = "domain name"
  default     = "david-oh.com"
}

# 
variable "subdomain" {
  description = "subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}