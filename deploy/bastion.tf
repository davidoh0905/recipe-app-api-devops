# "data" block is to retrieve things from AWS
data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    # How to filter for a certain AMI
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.20221210.1-x86_64-gp2"]
  }
  owners = ["amazon"] # Most optimized to run on AWS
}

variable "instance_type" {
  default = "t2.micro"
}

# Creating a new IAM role called "bastion" for our bastion instance
# Policy that allows us to assume this role. I don't know what that means...
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")
  tags               = local.common_tags
}

# Role that has assume role
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role = aws_iam_role.bastion.name
  # This is a predefined policy by AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

# Why the hell are there so many shit...
# What the fuck is instance profile...
# profile assigned to bastion when it launches
# And we assign role to that instance profile, which is the role that contains policy attached to it
# which policy is 
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

# Security groups control network access to and from the basion instance
# We want to allow SSH access to the bastion instance
# give any resource or server, locking down specific port for inbound and outbound access.
# only allow one port, attacker can only attack application running on that single port!
# ONE MACHINE & ONE PORT & ONE APPLICATION
# ONE MACHINE can have many ports and other applications could be running too.
# so only allow machine's port that you are specifically intending to expose!

# Create Security group to allow 
# Inbound Access to port 22(SSH) --> connect to instance
# Output Access to port 443(HTTP) & 80(HTTPS) 
# --> allow bastion to retrieve updates from package manager, which uses these two protocols
# Outbound access to port 5432(Postgres) --> allow bastion to connect to database
# QUESTION: Why do we need to allow outbound access to port 5432?
# - Can't we use bastion port 1234 and map that to Postgres instance port 5432?

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow SSH access from anywhere"
    # Allows inbound internet access into our port 22
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    # Allow access from anywhere. any IP address.
    # If your office uses static IP, you can replace this.
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound access from our server to the internet
  # HTTP and HTTPS both require for accessing package repository
  # in order to install updates on bastion server
  # also pulling down images from ECR.
  egress {
    description = "Allow outbound HTTPS"
    # Secure Protocol
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    # Allow access to anywhere
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    # Allow access to anywhere
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound Postgres"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    # Allow access to specified CIDR blocks for private a and b subnets!
    # Limit access to only subnets specified.
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}

# "resource" block is to create things in AWS
resource "aws_instance" "bastion" {
  # retrieved the id of AMI with "data" block 
  # block.type_of_resource.name_of_resource.id_of_resource
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = var.instance_type
  user_data            = file("./templates/bastion/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  # This will take the key name from the variable file.
  # And it should know how to look up the keyname and get the value of public ssh key
  key_name = var.bastion_key_name
  # Subnet we want our AWS instance to launch into
  # into public subnet because we want the bastion instance to be publicly available from internet.
  # connect from local machine and administer resource
  # why a? it's not critical service. only for admin. you can only launch in one subnet at a time.
  # for now, let's just keep it simple and launch in one subnet
  subnet_id              = aws_subnet.public_a.id
  vpc_security_group_ids = [aws_security_group.bastion.id]

  # This is so sick. use merge feature to just add more tags to local.common_tags
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

# assign new permissions tp CI User account
# An instance profile is something that we can assign to our
# bastion instance to give it IAM role information.
# to give IAM role information?? what does that mean?
# You don't just assign IAM role to an instance?

# passing instance profile to give instance ability to assume a role
# temporary AWS credentials...?
