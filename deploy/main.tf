# Cloud Agnostic
terraform {
  ## State File
  ## Backend for CICD tool
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-davidoh"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0" # This version matters
}


## Workspaces are ways you can manage different environments within the same AWS account
# Multiple Environments
# Dev Site, Staging Site, Production Site.
# Different sets of AWS resources in infrastructure.

## ** "Locals" are ways that you can create dynamic variables inside Terraform
locals {
  # Interpolation Syntax : allows you to concatenate strings from
  # different resources and variables within Terraform
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Contact     = var.contact
    ManagedBy   = "Terraform"
  }
}

# Retrieve the current region. No need to hardcode regions.
data "aws_region" "current" {}

