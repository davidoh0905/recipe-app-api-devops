# create data rosource 
data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}." # End with .
}

resource "aws_route53_record" "app" {
  # add subdomains to the zone
  zone_id = data.aws_route53_zone.zone.zone_id
  # lookup from variable.subdomain, the key of workspace.
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.api.dns_name]
}

resource "aws_acm_certificate" "cert" {
  # Fully Qualified Domain Name
  domain_name = aws_route53_record.app.fqdn
  # You need to validate that you own that domain
  validation_method = "DNS"

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]
  ttl = 60
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}

