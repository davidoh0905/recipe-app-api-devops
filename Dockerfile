FROM python:3.7-alpine
LABEL MAINTAINER = "London App Developer Ltd"

ENV PYTHONUNBUFFERED 1

## Set the path to the scripts folder in the container.
# Any Linux based container already has PATH environment variable.
# Sets the environment variable PATH to add "scripts" path to existing PATH
## This will enable us to run the scripts from anywhere in the container.
ENV PATH="/scripts:${PATH}"

# To get rid of warning that pip is out of date
RUN pip install --upgrade pip

COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
      gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt
RUN apk del .tmp-build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app

# Copy our script to the image and run it. Make it runnable after copying!
COPY ./scripts /scripts
RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user
# Fargate 1.4.0 has recently been released and introduces a new breaking change.
# The issue is the API app container will fail to start.
VOLUME /vol/web 

## Able to just give shortcut because we have added script PATH

## Everything before CMD is for build time
## CMD is run during runtime
## When multiline commands are needed for start up, use an entrypoint.sh file

## Modifying this to run entrypoint.sh is the necessary change to run this in production.
## docker run image 할때 실행되는거!
CMD ["entrypoint.sh"]
